from performance.app import create_app

class PrefixMiddleware:

    def __init__(self, app, prefix):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        if environ['PATH_INFO'].startswith(self.prefix):
            environ['PATH_INFO'] = environ['PATH_INFO'][len(self.prefix):]
            environ['SCRIPT_NAME'] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response('404', [('Content-Type', 'text/plain')])
            return ["This url does not belong to the app.".encode()]


app = create_app()

if app.env == 'development':
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, app.config['PREFIX'])
    @app.before_first_request
    def before_first_request():
        print(f" * Using prefix {app.config['PREFIX']!r}")
