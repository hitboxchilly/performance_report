import datetime as dt

from flask import Blueprint, redirect, render_template, url_for
from flask_login import current_user, fresh_login_required, login_required

from .report import Report

index_bp = Blueprint('index', __name__)

@index_bp.route('/')
def index():
    if current_user.is_authenticated:
        endpoint = 'editor'
    else:
        endpoint = 'login'
    return redirect(url_for(endpoint))

@index_bp.route('/editor')
@fresh_login_required
def editor():
    return render_template('editor.html')

@index_bp.route('/report/<int:year>/<int:month>/<int:day>')
@login_required
def report(year, month, day):
    date = dt.date(year, month, day)
    return render_template('report.html',
            report=Report.query.filter(Report.date == date).one())

@index_bp.route('/schedule')
@fresh_login_required
def schedule():
    return render_template('schedule.html')

@index_bp.route('/profile')
@fresh_login_required
def profile():
    return render_template('profile.html')

