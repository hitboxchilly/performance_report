import datetime as dt
from collections import OrderedDict

from flask import Blueprint, request
import flask_restful as restful
from flask_restful import Resource, fields, reqparse, marshal_with

from .extensions import api, db

api_bp = Blueprint('api', __name__, url_prefix='/api')

# TODO: these will be actual databases
REPORTS = {}
flights = {}

class _Date(fields.Raw):

    def format(self, value):
        try:
            return str(value)
        except Exception as e:
            raise fields.MarshallingException(e)

fields.Date = _Date


class ReportApi(Resource):

    fields = { 'date': fields.Date, }

    def __init__(self, db):
        self.db = db


class ReportList(Resource):

    def __init__(self, db):
        self.db = db
        self.post_parser = reqparse.RequestParser()
        self.post_parser.add_argument('id', required=True, help='Report primary key')
        self.post_parser.add_argument('date', required=True, help='Report date')

    def get(self):
        return REPORTS

    @marshal_with(ReportApi.fields)
    def post(self):
        # 409 Conflict on exists?
        # https://stackoverflow.com/a/3826024/2680592
        args = self.post_parser.parse_args()
        REPORTS[args.id] = { 'date': args.date, }
        return REPORTS[args.id]


class FlightResource(Resource):

    def __init__(self, db):
        self.db = db

    def get(self, flight_id):
        return {flight_id: flights[flight_id]}

    def put(self, flight_id):
        flights[flight_id] = request.form['data']
        return {flight_id: flights[flight_id]}


api.add_resource(ReportApi, '/report/<report_id>', endpoint='api.report',
                 resource_class_args=(db, ))
api.add_resource(ReportList, '/reports', endpoint='api.reports',
                 resource_class_args=(db, ))
api.add_resource(FlightResource, '/flights/<flight_id>', endpoint='api.flight',
                 resource_class_args=(db, ))
