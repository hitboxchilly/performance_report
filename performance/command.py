def init_app(app):
    """
    Convenient flask commands.
    """
    import click

    from .extensions import db
    from .datamigration import DataMigrate

    @app.cli.command()
    def initdb():
        """
        Create all models in the database and prompt to add an admin user.
        """
        # declare all the models
        from . import delay
        from . import flight
        from . import report
        from . import station
        from . import user
        db.create_all()

        username = click.prompt('admin username', default='admin')
        password = click.prompt('admin password', hide_input=True, confirmation_prompt=True)
        db.session.add(user.User(username=username, password=password, is_admin=True))
        db.session.commit()

    @app.cli.command()
    @click.argument('mdbpath')
    @click.option('--commit', is_flag=True, show_default=True, help='Commit the changes.')
    def data(mdbpath, commit):
        """
        Pull the old MDB data into this app's database.
        """
        migrater = DataMigrate(mdbpath)
        migrater.migrate()
        if commit:
            db.session.commit()
