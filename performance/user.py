from flask_login import UserMixin
from flask_wtf import FlaskForm
from sqlalchemy_utils import PasswordType
from wtforms import PasswordField, StringField, SubmitField
from wtforms_alchemy import ModelForm

from .extensions import db

class User(UserMixin, db.Model):

    __tablename__ = 'users'

    is_anonymouse = False

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String)
    password = db.Column(PasswordType(schemes=['pbkdf2_sha512']))
    is_admin = db.Column(db.Boolean, default=False)

    is_active = db.Column(db.Boolean, default=True)


class LoginForm(FlaskForm):
    """
    Login form.
    """
    username = StringField('username')
    password = PasswordField('password')
    submit = SubmitField('login')


class UserForm(ModelForm):
    """
    Form form manipulating actual User objects.
    """
    class Meta:
        model = User
