import argparse
import csv
import datetime as dt
import subprocess
from collections import defaultdict
from pathlib import Path

from .extensions import db

from .delay import Code
from .flight import Delay, Flight, FlightType, Leg
from .report import Report
from .station import Station

class mdbreader:

    def __init__(self, mdb):
        self.mdb = mdb

    def read_table(self, table):
        # working around the fact that there are \r\n in the memo fields but
        # the \r is lost somewhere in the call.
        lineterm = '\n' * 128
        cmd = ['mdb-export',
               '-D', '%Y-%m-%d %H:%M:%S',
               '-R', lineterm,
               self.mdb, table]
        output = subprocess.check_output(cmd, text=True)
        output = output.split(lineterm)
        return csv.DictReader(output)


def datefromiso(s):
    """
    Assumes the full ISO date/time format
    """
    return None if not s else dt.date.fromisoformat(s[:10])

def timefromiso(s):
    if not s or len(s) < 19:
        return None
    return dt.time.fromisoformat(s[-8:])

class DataMigrate:

    def __init__(self, mdbpath):
        self.mdb = Path(mdbpath)

    def migrate(self):
        raise NotImplementedError('Migration from the old MDBs is database specific')
