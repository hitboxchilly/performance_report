from .extensions import db

class Code(db.Model):
    """
    A delay code.
    """

    __tablename__ = 'codes'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    # TODO:
    # ABX/DHL controllable
