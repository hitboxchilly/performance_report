from flask import Blueprint, flash, render_template

def init_app(app):
    """
    Add hidden routes for development.
    """
    if app.env == 'development':
        @app.route('/flashall')
        def _flashall():
            """
            Hidden route for developing the flash messages.
            """
            flash('Info Message', 'info')
            flash('Success Message', 'success')
            flash('Warning Message', 'warning')
            flash('Error Message', 'error')
            flash('Multiline flash message\nLine 1\nLine 2', 'info')
            return render_template('flashall.html')
