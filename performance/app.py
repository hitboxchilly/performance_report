from flask import Flask, current_app

from . import api
from . import command
from . import debug
from . import extensions
from . import index
from . import shell

def create_app(**config):
    app = Flask(__name__, instance_relative_config=True)

    app.config.setdefault('PREFIX', '/performance')
    app.config.from_pyfile('config.py')
    app.config.update(**config)

    app.config.setdefault('TITLE', 'TITLE')
    app.config.setdefault('DATEFMT', '%d-%B-%y')

    command.init_app(app)
    extensions.init_app(app)
    shell.init_app(app)
    debug.init_app(app)

    app.register_blueprint(api.api_bp)
    app.register_blueprint(index.index_bp)

    @app.context_processor
    def inject():
        return {
            'title': current_app.config['TITLE'],
        }

    return app
