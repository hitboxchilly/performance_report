def init_app(app):

    @app.shell_context_processor
    def shell_context_processor():
        """
        Add convenient things to the flask shell.
        """
        import datetime as dt

        import sqlalchemy as sa
        import sqlalchemy_utils as sa_utils

        from .extensions import admin, api, db, login_manager
        context = locals()

        # add all the models
        context.update(db.Model._decl_class_registry)

        return context
