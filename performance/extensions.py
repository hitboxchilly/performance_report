from flask import flash
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_assets import Bundle, Environment
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

admin = Admin()
api = Api(prefix='/api')
assets = Environment()
db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()

def init_app(app):
    base_bundle = Bundle('base.css', filters='cssmin', output='gen/packed.css')
    assets.register('base.css', base_bundle)
    assets.register('knockout.js', Bundle('knockout.js',
        filters='jsmin', output='gen/packed.js'))
    assets.register('editor.css', base_bundle, Bundle('editor.css',
        filters='cssmin', output='gen/editor.packed.css'))

    api.init_app(app)
    assets.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    init_admin(app)
    init_login_manager(app)

def init_admin(app):
    admin.init_app(app)

    from . import delay
    from . import flight
    from . import report
    from . import station
    from . import user

    admin.add_view(ModelView(delay.Code, db.session))
    admin.add_view(ModelView(flight.Flight, db.session))
    admin.add_view(ModelView(flight.Leg, db.session))
    admin.add_view(ModelView(flight.Delay, db.session))
    admin.add_view(ModelView(flight.FlightType, db.session))
    admin.add_view(ModelView(report.Report, db.session))
    admin.add_view(ModelView(station.Station, db.session))
    admin.add_view(ModelView(user.User, db.session))

def init_login_manager(app):
    login_manager.init_app(app)
    login_manager.login_view = 'login'
    login_manager.login_message_category = 'warning'

    init_login_routes(app)

def init_login_routes(app):
    from flask import redirect, render_template, url_for
    from flask_login import login_required, login_user, logout_user

    from .user import LoginForm, User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        form = LoginForm()

        if form.validate_on_submit():
            user = User.query.filter(User.username == form.username.data).one_or_none()
            if user is None:
                flash('Invalid', 'error')
            elif user.password == form.password.data:
                login_user(user)
                return redirect(url_for('index'))

        return render_template('login.html', form=form)

    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return redirect(url_for('index'))
