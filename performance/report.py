from .extensions import db

class Report(db.Model):

    __tablename__ = 'reports'

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), server_default=db.func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=db.func.now())

    # THINK
    # 1. make a mainline branch without the performance numbers branch DHL and
    #    Amazon with them.
    # 2. make a json field that contains the structure of the performance
    #    numbers. then how to do types?

    date = db.Column(db.Date)
    flights = db.relationship('Flight', backref='report')
    system_detail = db.Column(db.Text)
