from .extensions import db
from wtforms_alchemy import ModelForm

class Station(db.Model):

    __tablename__ = 'stations'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String, index=True, unique=True)

    def __repr__(self):
        return f'{self.__class__.__name__}(code={self.code!r})'


class StationForm(ModelForm):
    class Meta:
        model = Station
