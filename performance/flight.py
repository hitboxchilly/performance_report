import datetime as dt

from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.orderinglist import ordering_list

from .extensions import db

class FlightType(db.Model):
    """
    Valid types of flights. Scheduled and extra.
    """

    __tablename__ = 'flighttypes'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    # DHL has
    #
    # 1,"Scheduled",
    # 2,"Extra-CMI AMZ",
    # 3,"Extra Non-CMI AMZ",
    #
    #
    # Amazon has
    #
    # 1,"Scheduled"
    # 2,"Extra"
    # 3,"Extra CMI"
    # 4,"Extra Non-CMI"


class Flight(db.Model):

    __tablename__ = 'flights'

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), server_default=db.func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=db.func.now())

    report_id = db.Column(db.Integer, db.ForeignKey('reports.id'))

    flight_type_id = db.Column(db.Integer, db.ForeignKey('flighttypes.id'))
    flight_type = db.relationship('FlightType')

    flight_number = db.Column(db.String)
    leg = db.Column(db.Integer, server_default=db.text('1'))
    tail_number = db.Column(db.String)
    weight = db.Column(db.Integer)
    comment = db.Column(db.Text)

    origin_leg_id = db.Column(db.Integer, db.ForeignKey('legs.id'))
    origin_leg = db.relationship('Leg', backref='flight_origin',
            foreign_keys=[origin_leg_id])

    destination_leg_id = db.Column(db.Integer, db.ForeignKey('legs.id'))
    destination_leg = db.relationship('Leg', backref='flight_destination',
            foreign_keys=[destination_leg_id])


class Leg(db.Model):

    __tablename__ = 'legs'

    id = db.Column(db.Integer, primary_key=True)
    station_id = db.Column(db.Integer, db.ForeignKey('stations.id'))
    station = db.relationship('Station')
    estimated_date = db.Column(db.Date)
    estimated_time = db.Column(db.Time(timezone=True))
    actual_date = db.Column(db.Date)
    actual_time = db.Column(db.Time(timezone=True))
    delays = db.relationship('Delay', backref='leg', order_by='Delay.position',
            collection_class=ordering_list('position'))

    def estimated_actual_minutes(self):
        estimated = dt.datetime.combine(self.estimated_date, self.estimated_time)
        actual = dt.datetime.combine(self.actual_date, self.actual_time)

        if actual == estimated:
            return 0
        elif actual > estimated:
            td = actual - estimated
            sign = 1
        elif actual < estimated:
            td = estimated - actual
            sign = -1

        return int((td.seconds / 60) * sign)

    def delay_codes_for_report(self):
        return ' '.join(
            f'{delay.code.name}' f'({delay.minutes})' if delay.minutes else ''
            for delay in self.delays)


class Delay(db.Model):
    """
    Associate a Leg objects with Delay objects.
    """

    __tablename__ = 'delays'

    flightleg_id = db.Column(db.Integer, db.ForeignKey('legs.id'), primary_key=True)
    # ordering for delay
    position = db.Column(db.Integer, primary_key=True)

    code_id = db.Column(db.Integer, db.ForeignKey('codes.id'))
    code = db.relationship('Code')
    minutes = db.Column(db.Integer)
